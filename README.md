# README #

ObjcUtils is a framework written in Objective-C which aims at providing miscellaneous utilities and bug fixes over the standard Objective-C frameworks. In reality, it consists of a collection of utilities that solve some issues I have faced myself during development, which is why it's target at (and tested only with) iOS. Features are only included if the solutions they provide are considered good enough, as many times mediocre to poor solutions are required but, in such cases, I might prefer not to include them.

Very recently I faced a bug in the UIKit framework which displays different types of bullets when typing and not, if using custom fonts on secure text entries. I intensively googled solutions for this, as the bug made the UI look somewhat ugly and buggy, but I found none that I really liked. As such, I came up with my own solution, which I think is better than any other I saw, and so I decided to share. As such, the intent of this framework is not to provide a complete framework, but to share bug fixes and utilities as I face them. Don't expect timelines, tests, or support guarantees.

This framework is protected under the MIT license.

### Feature list ###

* `UTILTextField`, a `UITextField` subclass, which fixes the bullet size bug of when editing secure text entries with custom fonts
* `UTILTextView`, a `UITextView` subclass, which is capable of self adjusting its height or width to fit its contents
* `UTILDispatch` for dispatching blocks after a given delay, like `dispatch_after`, but with cancelation capabilities
* `NSString` obfuscation
* `UITextField` text selection and cursor positioning
* `CALayer` category which allows setting border color from Interface Builder
* `UIStoryboard` category for getting the main storyboard instance

### How do I get set up? ###

Other than creating a Cocoa Touch Framework project, I haven't done any setup at all to deploy the project as a framework, and I won't anytime soon. I recommend actually downloading the source and add the files to your project, or browse the files and copy paste whatever source you require. Somewhere in the future I intend to add support for CocoaPods, or some other installation mechanism of the kind.

### Contribution guidelines ###

I have never administrated a public project, so any tips are appreciated!

### Who do I talk to? ###

Project admin
André Fratelli
(andre.fratelli@hotmail.com)