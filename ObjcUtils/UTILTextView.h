//
//  UTILTextView.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    // Either width or height are allowed to be constrained, not both. One must be previously set so the other can be calculated. UTILTextViewConstraintModeNone uses no frame constraint
    UTILTextViewConstraintModeNone = 0,
    UTILTextViewConstraintModeHeight,
    UTILTextViewConstraintModeWidth,
    
} UTILTextViewConstraintMode;

@interface UTILTextView : UITextView

// The frame constraint mode indicates which frame attribute (width, height, or none) to be constrained. Default is UTILTextViewConstraintNone. The view is not forced to layout when the constraint type is set, so it's possible to animate it by laying out the view afterwards. This property can be used using Interface Builder by setting it as a runtime attribute, although doing so is not recommended (because its value is an enum, and it's not recommended to rely on its integer value). Notice that this feature does not promise future backwards compatibility. See the source file for details.
@property (nonatomic) UTILTextViewConstraintMode frameConstraintMode;

// Constraint used for either width or height. The constrained attribute will be computed so the view is resized to fit its contents. The recommended way to use this feature is to setup a width/height constraint in Interface Builder and check the property "Remove at build time". At runtime, usually at viewWillAppear:, set frameConstraintMode to whatever constraint mode is needed and the constraint will be automatically added.
@property (nonatomic, strong, readonly) NSLayoutConstraint * frameConstraint;

@end
