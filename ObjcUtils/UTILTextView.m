//
//  UTILTextView.m
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UTILTextView.h>

@implementation UTILTextView

@synthesize frameConstraintMode = _frameConstraintMode;
@synthesize frameConstraint = _frameConstraint;

- (void)setFrameConstraintMode:(UTILTextViewConstraintMode)frameConstraintMode
{
    if (_frameConstraintMode != frameConstraintMode) {
        
        // Remove the previous constraint, if any. I'm not sure whether the constraint should be removed before or after calculating the new constraint, so I'm postponing this decision to a future release and decide after getting public feedback. This can, however, compromise backwards compatibility if the decision favors differently from what is already implemented. Do not rely on this feature for now.
        if (_frameConstraintMode != UTILTextViewConstraintModeNone) {
            [self removeConstraint:self.frameConstraint];
        }
        
        // Create the constraint
        if (frameConstraintMode == UTILTextViewConstraintModeWidth) {
            _frameConstraint = [self widthFrameConstraint];
        }
        
        else if (frameConstraintMode == UTILTextViewConstraintModeHeight) {
            _frameConstraint = [self heightFrameConstraint];
        }
        
        // Update the view's constraints
        if (frameConstraintMode != UTILTextViewConstraintModeNone) {
            
            [self addConstraint:_frameConstraint];
            [self setNeedsUpdateConstraints];
        }
        
        _frameConstraintMode = frameConstraintMode;
    }
}

- (NSLayoutConstraint *)heightFrameConstraint
{
    CGSize size = [self sizeThatFits:CGSizeMake(self.bounds.size.width, FLT_MAX)];
    
    // Height frame constraint
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeHeight
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:nil
                                        attribute:0
                                       multiplier:1.0f
                                         constant:size.height];
}

- (NSLayoutConstraint *)widthFrameConstraint
{
    CGSize size = [self sizeThatFits:CGSizeMake(FLT_MAX, self.bounds.size.height)];
    
    // Width frame constraint
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeWidth
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:nil
                                        attribute:0
                                       multiplier:1.0f
                                         constant:size.width];
}

@end
