//
//  NSString+Utils.m
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <NSString+UTIL.h>

@implementation NSString (UTIL)

- (NSString *)replaceWithString:(NSString *)string
                        inRange:(NSRange)range
{
    NSMutableString * result = [NSMutableString stringWithString:self];
    
    // Delete characters in range and insert the new ones
    [result deleteCharactersInRange:range];
    [result insertString:string atIndex:range.location];
    
    return [NSString stringWithString:result];
}

- (NSString *)obfuscateReplacingWithString:(NSString *)replacement
                                   inRange:(NSRange)range
                               placeholder:(NSString *)placeholder
{
    NSMutableString * mutableString;
    NSString * obfuscatedString;
    
    // Delete characters in range
    mutableString = [NSMutableString stringWithString:self];
    [mutableString deleteCharactersInRange:range];
    
    // Obfuscate
    obfuscatedString = [mutableString obfuscateUsingPlaceholder:placeholder];
    
    // Insert the remaining characters
    mutableString = [NSMutableString stringWithString:obfuscatedString];
    [mutableString insertString:replacement atIndex:range.location];
    
    return [NSString stringWithString:mutableString];
}

- (NSString *)obfuscateUsingPlaceholder:(NSString *)placeholder
{
    // Repeat the placeholder as many times as the string's length
    return [@"" stringByPaddingToLength:self.length
                             withString:placeholder
                        startingAtIndex:0];
}

@end
