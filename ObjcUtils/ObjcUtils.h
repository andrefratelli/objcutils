//
//  ObjcUtils.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ObjcUtils.
FOUNDATION_EXPORT double ObjcUtilsVersionNumber;

//! Project version string for ObjcUtils.
FOUNDATION_EXPORT const unsigned char ObjcUtilsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ObjcUtils/PublicHeader.h>


