//
//  UIStoryboard+ObjcUtils.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (UTIL)

// Getter for the main storyboard for the main bundle, as defined by project settings. Uses the key UIMainStoryboardFile from the main bundle to get the main storyboard.
+ (UIStoryboard *)mainStoryboard;

@end
