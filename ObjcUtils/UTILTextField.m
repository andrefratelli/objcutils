//
//  UTILTextField.m
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UTILTextField.h>
#import <UTILDispatch.h>
#import <NSString+UTIL.h>
#import <UITextField+UTIL.h>

@interface UTILTextField () <UITextFieldDelegate>

// Replacement delegate. To fix the bullet size bug when using custom fonts, instances of this class are required to set themselves as delegates. Although this way it's possible to fix the bug, it means that the class would then lack delegate support (there can only be one delegate). To still provide support for the standard delegate, instead an intermediary delegate is used, which forwards calls to the delegate set by the caller.
@property (nonatomic, assign) id<UITextFieldDelegate> actualDelegate;

// Replacement for the text field's text, which always represents the actual text. Obfuscation occurs in the parent's text attribute, while this attribute is used to keep track of whatever as been typed in the field.
@property (nonatomic, strong) NSString * actualText;

// Dispatcher which fully obfuscates protected text entries after a certain delay
@property (nonatomic, strong) UTILDispatch * obfuscationDispatcher;

@end

@implementation UTILTextField

@synthesize protectedTextEntry = _protectedTextEntry;
@synthesize protectedTextPlaceholder = _protectedTextPlaceholder;
@synthesize obfuscationDelay = _obfuscationDelay;
@synthesize actualDelegate = _actualDelegate;
@synthesize actualText = _actualText;
@synthesize obfuscationDispatcher = _obfuscationDispatcher;

- (void)setText:(NSString *)text
{
    self.actualText = text;
}

- (NSString *)text
{
    return self.protectedTextEntry ? self.actualText : super.text;
}

- (void)setActualText:(NSString *)actualText
{
    // Save the text
    _actualText = super.text = actualText;
    
    // Obfuscate if needed
    [self obfuscateIfProtected];
    
    // Clear the obfuscation dispatcher when the text changes or the results become buggy. It would cause text to appear after the delay completes, if the field is no longer protected and its contents changed in the meanwhile
    self.obfuscationDispatcher = nil;
}

- (NSString *)actualText
{
    // actualText defaults to the empty string
    if (_actualText == nil) {
        _actualText = @"";
    }
    
    return _actualText;
}

- (void)setProtectedTextEntry:(BOOL)protectedTextEntry
{
    if (_protectedTextEntry != protectedTextEntry) {
        _protectedTextEntry = protectedTextEntry;
        
        // When protecting the text entry the instance needs to listen to delegate events
        if (self.protectedTextEntry) {
            self.actualDelegate = super.delegate;
            super.delegate = self;
        }
        
        else {
            super.delegate = self.actualDelegate;
            self.actualDelegate = nil;
        }
        
        // Store the cursor position so it's kept when toggling the view
        UITextRange * range = [self selectedTextRange];
        
        // When protecting, obfuscate the code
        [self obfuscateIfProtected];
        
        // When not protecting, show the actual text
        if (!self.protectedTextEntry) {
            self.text = self.actualText;
        }
        
        // Restore the cursor's position
        [self setSelectedTextRange:range];
    }
}

- (id<UITextFieldDelegate>)delegate
{
    return self.protectedTextEntry ? self.actualDelegate : self.delegate;
}

- (void)setDelegate:(id<UITextFieldDelegate>)delegate
{
    if (self.protectedTextEntry) {
        self.actualDelegate = delegate;
    }
    
    else {
        super.delegate = delegate;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([self.actualDelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        return [self.actualDelegate textFieldShouldBeginEditing:textField];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.actualDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.actualDelegate textFieldDidBeginEditing:textField];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if ([self.actualDelegate respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
        return [self.actualDelegate textFieldShouldEndEditing:textField];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.actualDelegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.actualDelegate textFieldDidEndEditing:textField];
    }
    
    // Obfuscate when done editing
    [self obfuscateIfProtected];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if ([self.actualDelegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.actualDelegate textFieldShouldClear:textField];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.actualDelegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [self.actualDelegate textFieldShouldReturn:textField];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Notify the actual delegate and respect its decision (if it's NO)
    if ([self.actualDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        if (![self.actualDelegate textField:textField shouldChangeCharactersInRange:range replacementString:string]) {
            return NO;
        }
    }
    
    // Only protected text entries should proceed, otherwise the change is accepted
    if (!self.protectedTextEntry) {
        return YES;
    }
    
    // Cache the text to perform both changes, keeping the cursor position and selection range
    NSString * text = self.actualText;
    NSRange selectionRange = [self selectedTextNSRange];
    
    // Perform the changes to the actual text
    self.actualText = [text replaceWithString:string
                                      inRange:range];
    
    // Obfuscate for display
    NSString * obfuscated = [text obfuscateReplacingWithString:string
                                                       inRange:range
                                                   placeholder:[self protectedTextPlaceholder]];
    
    [super setText:obfuscated];
    
    // When changing in an entermidiary position the cursor jumps to the end, so we need to replace it where it was
    NSUInteger offset = (selectionRange.length == 1 ? 1 : 0) - (range.length > 1 ? 0 : range.length);
    
    selectionRange.location = selectionRange.location + string.length + offset;
    selectionRange.length = 0;
    
    [self setSelectedTextNSRange:selectionRange];
    
    // Obfuscate
    __weak UTILTextField * weakSelf = self;
    void (^obfuscate)() = ^{
        
        UTILTextField * strongSelf = weakSelf;
        UITextRange * range = [strongSelf selectedTextRange];
        
        // Obfuscate and replace the cursor
        [strongSelf obfuscateIfProtected];
        [strongSelf setSelectedTextRange:range];
        
    };
    
    // Schedule obfuscation
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.obfuscationDelay * NSEC_PER_SEC));
    UTILDispatch * dispatcher = [UTILDispatch dispatch_after:delay
                                                       queue:dispatch_get_main_queue()
                                                       block:obfuscate];
    
    self.obfuscationDispatcher = dispatcher;
    
    return NO;
}

- (void)setObfuscationDispatcher:(UTILDispatch *)obfuscationDispatcher
{
    // Cancel any previous obfuscator
    if (_obfuscationDispatcher != nil) {
        [_obfuscationDispatcher cancel];
    }
    
    // Keep a reference for the next obfuscator
    _obfuscationDispatcher = obfuscationDispatcher;
}

- (NSString *)protectedTextPlaceholder
{
    if (_protectedTextPlaceholder == nil) {
        
        // @"\u2022" - Small bullet
        // @"\u25CF" - Large bullet
        return @"\u25CF";
    }
    
    return _protectedTextPlaceholder;
}

- (int64_t)obfuscationDelay
{
    // Defaults to 2, and zero sets it to the default. Notice that text fields should always have delay when obfuscating, otherwise the user cannot see what he's typing
    if (_obfuscationDelay == 0) {
        return 2;
    }
    
    return _obfuscationDelay;
}

- (void)obfuscateIfProtected
{
    // Obfuscate if protected
    if (self.protectedTextEntry) {
        super.text = [self.actualText obfuscateUsingPlaceholder:[self protectedTextPlaceholder]];
    }
}

@end
