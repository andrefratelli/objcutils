//
//  UITextField+ObjcUtils.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (UTIL)

// Allows setting text selection using NSRange instances instead of UITextRange. These methods convert UITextRange from and to NSRange and perform the same task using UITextRange instances instead.
- (void)setSelectedTextNSRange:(NSRange)range;
- (NSRange)selectedTextNSRange;

@end


