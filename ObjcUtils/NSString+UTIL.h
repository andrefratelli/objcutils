//
//  NSString+Utils.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UTIL)

// Replaces the range in self with the given string. range.location indicates where to start replacing and range.length indicates how many characters to delete from the original string. No range checking is performed, so out-of-bounds exceptions might occur. The original string remains unchanged, a copy is returned.
- (NSString *)replaceWithString:(NSString *)string
                        inRange:(NSRange)range;

// Obfuscates the string, inserting the replacement string only after obfuscation. The original string is obfuscated, but the newly inserted characters are not. This is mostly useful for password obfuscation, as the standard behaviour is to show the last inserted character for a short period of time. The placeholder is not necessarily one character long. No range checking is performed, so out-of-bounds exceptions might occur. The original string remains unchanged, a copy is returned.
- (NSString *)obfuscateReplacingWithString:(NSString *)replacement
                                   inRange:(NSRange)range
                               placeholder:(NSString *)placeholder;

// Obfuscates the string with the given placeholder, which is to say that the placeholder is repeated as many times as the string's length. The original string remains unchanged, a copy is returned.
- (NSString *)obfuscateUsingPlaceholder:(NSString *)placeholder;

@end
