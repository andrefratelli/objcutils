//
//  UTILDispatch.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>
#import <dispatch/time.h>

@interface UTILDispatch : NSObject

// Whether the dispatch has been canceled
@property (nonatomic, readonly) BOOL canceled;

// Schedule a callback with cancelation capabilities
+ (UTILDispatch *)dispatch_after:(dispatch_time_t)when
                           queue:(dispatch_queue_t)queue
                           block:(dispatch_block_t)block;

// Cancels the dispatched event
- (void)cancel;

@end
