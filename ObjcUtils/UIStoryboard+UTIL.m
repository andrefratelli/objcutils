//
//  UIStoryboard+ObjcUtils.m
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UIStoryboard+UTIL.h>

@implementation UIStoryboard (UTILS)

+ (UIStoryboard *)mainStoryboard
{
    NSBundle * bundle = [NSBundle mainBundle];
    NSString * storyboardName = [bundle objectForInfoDictionaryKey:@"UIMainStoryboardFile"];
    
    return [UIStoryboard storyboardWithName:storyboardName bundle:bundle];
}

@end
