//
//  UITextField+ObjcUtils.m
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UITextField+UTIL.h>

@implementation UITextField (UTIL)


- (void)setSelectedTextNSRange:(NSRange)range
{
    UITextPosition * start = [self positionFromPosition:[self beginningOfDocument]
                                                 offset:range.location];
    UITextPosition * end = [self positionFromPosition:start
                                               offset:range.length];
    UITextRange * textRange = [self textRangeFromPosition:start
                                               toPosition:end];
    
    [self setSelectedTextRange:textRange];
}

- (NSRange)selectedTextNSRange
{
    UITextRange * range = self.selectedTextRange;
    NSInteger location = [self offsetFromPosition:self.beginningOfDocument
                                       toPosition:range.start];
    NSInteger length = [self offsetFromPosition:range.start
                                     toPosition:range.end];
    
    return NSMakeRange(location, length);
}

@end
