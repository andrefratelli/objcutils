//
//  UTILDispatch.m
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UTILDispatch.h>

@implementation UTILDispatch

@synthesize canceled = _canceled;

+ (UTILDispatch *)dispatch_after:(dispatch_time_t)when
                           queue:(dispatch_queue_t)queue
                           block:(dispatch_block_t)block
{
    // Avoid trapping the dispatch in the block below to prevent retain cycles
    UTILDispatch * dispatch = [[UTILDispatch alloc] init];
    __weak UTILDispatch * weakDispatch = dispatch;
    
    dispatch_after(when, queue, ^{
        
        // Don't proceed when canceled
        if (weakDispatch.canceled) {
            return;
        }
        
        // Execute the dispatched block
        block();
    });
    
    return dispatch;
}

- (void)cancel
{
    _canceled = YES;
}

@end
