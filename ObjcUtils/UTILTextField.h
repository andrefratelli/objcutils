//
//  UTILTextField.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTILTextField : UITextField

// Works as a replacement for the standard secureTextEntry property, while fixing the bug when using custom fonts. This bug causes the text field to show different bullet sizes when editing and not, if using custom fonts. This value can be toggled at any time to remove the field's protection.
@property (nonatomic) BOOL protectedTextEntry;

// The placeholder to use when protecting text. Any unicode string is valid, although single bullets or asteriscs are more common. The standard is to use either @"\u2022" (Small bullet) or @"\u25CF" (Large bullet). The default is the former.
@property (nonatomic, strong) NSString * protectedTextPlaceholder;

// Delay to use when obfuscating protected text. When typing, the last typed character is show for as many seconds as indicated by this attribute, then it's also abfuscated. The default delay is two seconds. As it's not recommended to use no delay at all - as it would prevent the user from seeing what is being typed - setting this property to zero sets it to the default value (again, two seconds).
@property (nonatomic) int64_t obfuscationDelay;

// Obfuscates the text if the text field is protected and does nothing otherwise. The obfuscation delay is not respected by this method, as the entire string is obfuscated. Use protectedTextPlaceholder to configure the placeholder when obfuscating the string.
- (void)obfuscateIfProtected;

@end
