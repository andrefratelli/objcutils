//
//  CALayer+ObjcUtils.h
//  ObjcUtils
//
//  Created by André Fratelli on 16/12/14.
//  Copyright (c) 2014 André Fratelli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer (UTIL)

// Allows setting the layer's border color using UIColor instances instead of CGColor. This is mostly useful for setting border colors directly in interface builder, using runtime attributes. To do sow, add a runtime attribute called borderUIColor of type Color.
- (void)setBorderUIColor:(UIColor *)color;
- (UIColor *)borderUIColor;

@end
